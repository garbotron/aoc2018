let input = System.IO.File.ReadAllLines "day-01-input.txt" |> Seq.map int

let repeated = seq { while true do yield! input }
let sums = repeated |> Seq.scan (+) 0
let seen = sums |> Seq.scan (fun xs x -> Set.add x xs) Set.empty

printfn "Part 1: %d" (input |> Seq.sum)
printfn "Part 2: %d" (sums |> Seq.zip seen |> Seq.find (fun (s, x) -> s |> Set.contains x) |> snd)
