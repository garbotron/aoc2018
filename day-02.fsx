let input = System.IO.File.ReadAllLines "day-02-input.txt" |> Array.toList

let letterCount ch str = str |> Seq.filter ((=) ch) |> Seq.length
let hasletters count str = ['a'..'z'] |> List.exists (fun x -> letterCount x str = count)
let checksum strs =
  let has2 = strs |> Seq.filter (hasletters 2) |> Seq.length
  let has3 = strs |> Seq.filter (hasletters 3) |> Seq.length
  has2 * has3

let diff s1 s2 =
  s1 |> Seq.zip s2 |> Seq.filter (fun (x, y) -> x = y) |> Seq.map fst |> Seq.toArray |> System.String

printfn "Part 1: %d" (input |> checksum)

List.allPairs input input
  |> List.map (fun (x, y) -> x, y, diff x y)
  |> List.find (fun (x, _, z) -> z.Length = x.Length - 1)
  |> fun (_, _, z) -> z
  |> printfn "Part 2: %s"
