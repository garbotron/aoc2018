open System.Text.RegularExpressions

type Claim =
  { ID: int; X: int; Y: int; W: int; H: int }
  member this.R = this.X + this.W - 1
  member this.B = this.Y + this.H - 1

let parseClaim str =
  let m = Regex.Match(str, "#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)")
  assert m.Success
  let grp (i: int) = m.Groups.[i].Value |> int
  { ID = grp 1; X = grp 2; Y = grp 3; W = grp 4; H = grp 5 }

let input = System.IO.File.ReadAllLines "day-03-input.txt" |> Array.toList |> List.map parseClaim

let inside (x, y) claim = x >= claim.X && y >= claim.Y && x <= claim.R && y <= claim.B
let allCoords = List.allPairs [0 .. 999] [0 .. 999]
let claimCoords claim = List.allPairs [claim.X .. claim.R] [claim.Y .. claim.B]
let claimCount coords = input |> Seq.filter (inside coords) |> Seq.length
let isNonOverlapping claim = claim |> claimCoords |> List.map claimCount |> List.forall ((=) 1)

printfn "Part 1: %d" (allCoords |> Seq.filter (fun x -> claimCount x > 1) |> Seq.length)
printfn "Part 2: %d" (input |> Seq.find isNonOverlapping |> fun x -> x.ID)
