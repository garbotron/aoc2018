open System
open System.Text.RegularExpressions

type RecordAction = WakeUp | FallAsleep | BeginShift of int
type Record = DateTime * RecordAction

let parseRecord str =
  let m = Regex.Match(str, @"\[(.*)\] (wakes up|falls asleep|Guard #(.*) begins shift)")
  assert m.Success
  let time = m.Groups.[1].Value |> DateTime.Parse
  match m.Groups.[2].Value with
  | "wakes up" -> time, WakeUp
  | "falls asleep" -> time, FallAsleep
  | _ -> time, BeginShift (m.Groups.[3].Value |> int)

let input = System.IO.File.ReadAllLines "day-04-input.txt" |> Array.toList |> List.map parseRecord

type SleepPeriod = { Guard: int; Asleep: int; Awake: int }

let sleepPeriods = List.ofSeq <| seq {
  let mutable guard = 0
  let mutable asleep = 0
  for record in input |> List.sortBy fst do
    match record with
    | _, BeginShift g -> guard <- g
    | t, FallAsleep -> asleep <- t.Minute
    | t, WakeUp -> yield { Guard = guard; Asleep = asleep; Awake = t.Minute }
}

let allGuards = sleepPeriods |> List.map (fun x -> x.Guard) |> List.distinct
let sleepPeriodsByGuard = sleepPeriods |> List.groupBy (fun x -> x.Guard) |> Map
let totalSleepLength guard = sleepPeriodsByGuard |> Map.find guard |> Seq.sumBy (fun x -> x.Awake - x.Asleep)
let isAsleep minute period = minute >= period.Asleep && minute < period.Awake
let daysAsleep guard minute = sleepPeriodsByGuard |> Map.find guard |> Seq.filter (isAsleep minute) |> Seq.length
let sleepiestGuard = allGuards |> List.maxBy totalSleepLength
let sleepiestMinute = [0 .. 59] |> List.maxBy (daysAsleep sleepiestGuard)

printfn "Part 1: %d" (sleepiestGuard * sleepiestMinute)

List.allPairs allGuards [0 .. 59]
  |> List.maxBy (fun (x, y) -> daysAsleep x y)
  |> fun (x, y) -> x * y
  |> printfn "Part 2: %d"
