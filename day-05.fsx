let input = System.IO.File.ReadAllText "day-05-input.txt" |> fun x -> x.Trim()

let equalIgnoreCase x y = System.Char.ToLower x = System.Char.ToLower y
let equalWithDiffCase x y = x <> y && equalIgnoreCase x y

let rec react (str: string) =
  let mutable finished = true
  let rec append i (sb: System.Text.StringBuilder) =
    if i = str.Length then
      sb.ToString()
    elif i < str.Length - 1 && equalWithDiffCase str.[i] str.[i + 1] then
      finished <- false
      append (i + 2) sb
    else
      append (i + 1) (sb.Append str.[i])
  let ret = append 0 (System.Text.StringBuilder())
  if finished then ret else react ret

printfn "Part 1: %d" (input |> react |> Seq.length)

let allPolymers = ['a'..'z'] |> List.map string
let removePolymer (polymer: string) (str: string) =
  str.Replace(polymer, "", System.StringComparison.CurrentCultureIgnoreCase)

allPolymers
  |> List.map (fun p -> input |> removePolymer p |> react)
  |> List.map (fun s -> s.Length)
  |> List.min
  |> printfn "Part 2: %d"
