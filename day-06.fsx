let input =
  System.IO.File.ReadAllLines "day-06-input.txt"
  |> Array.toList
  |> List.map (fun s -> s.Split ", " |> fun x -> int x.[0], int x.[1])

let minX = input |> List.map fst |> List.min
let maxX = input |> List.map fst |> List.max
let minY = input |> List.map snd |> List.min
let maxY = input |> List.map snd |> List.max

let onEdge (x, y) = x = minX || x = maxX || y = minY || y = maxY

let allCoords = [minX .. maxX] |> List.collect (fun x -> [minY .. maxY] |> List.map (fun y -> x, y))

let dist (x, y) (x', y') = abs (x - x') + abs (y - y')

let closest pt =
  let targets = input |> List.map (fun x -> x, dist x pt) |> List.sortBy snd
  if (snd targets.[0]) = (snd targets.[1]) then None else Some (fst targets.[0])

let foldClosest map pt =
  match closest pt with
  | None -> map
  | Some x -> map |> Map.add x (pt :: (map |> Map.find x))

let targetMap = allCoords |> List.fold foldClosest (input |> List.map (fun x -> x, []) |> Map)
let finiteTargets = input |> List.filter (fun x -> targetMap |> Map.find x |> List.forall (onEdge >> not))

printfn "Part 1: %d" (finiteTargets |> List.map ((fun x -> targetMap |> Map.find x) >> List.length) |> List.max)

let inSafeRegion pt = (input |> List.sumBy (dist pt)) < 10000

printfn "Part 2: %d" (allCoords |> List.filter inSafeRegion |> List.length)
