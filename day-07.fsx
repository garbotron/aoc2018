open System.Text.RegularExpressions

let parseLine str =
  let m = Regex.Match(str, "Step ([A-Z]) must be finished before step ([A-Z]) can begin")
  char m.Groups.[1].Value, char m.Groups.[2].Value

let input = System.IO.File.ReadAllLines "day-07-input.txt" |> Array.toList |> List.map parseLine

let allSteps = input |> List.collect (fun (x, y) -> [x; y]) |> Set

let rec calcP1 remaining taken =
  if remaining |> Set.isEmpty then
    ""
  else
    let isStepLegal s = input |> List.forall (fun (x, y) -> y <> s || (taken |> Set.contains x))
    let legalSteps = remaining |> Set.filter isStepLegal
    let chosenStep = legalSteps |> Seq.min
    (chosenStep |> string) + calcP1 (remaining |> Set.remove chosenStep) (taken |> Set.add chosenStep)

printfn "Part 1: %s" (calcP1 allSteps Set.empty)

type WorkerState = NotWorking | Working of char * int

let calcP2 workerCount overhead =
  let mutable taken: Set<char> = Set.empty
  let mutable remaining = allSteps
  let mutable time = 0
  let mutable workers = List.init workerCount (fun i -> i, NotWorking) |> Map
  let isStepLegal s = input |> List.forall (fun (x, y) -> y <> s || (taken |> Set.contains x))
  let timeForStep s = (int s - int 'A') + overhead

  while (not remaining.IsEmpty) || (workers |> Map.exists (fun _ x -> x <> NotWorking)) do
    // Assign any free workers with new jobs.
    let mutable legalSteps = remaining |> Set.filter isStepLegal
    while (workers |> Map.exists (fun _ x -> x = NotWorking)) && not (legalSteps |> Set.isEmpty) do
      let worker = workers |> Map.filter (fun _ x -> x = NotWorking) |> Map.toSeq |> Seq.head |> fst
      let step = legalSteps |> Seq.head
      workers <- workers |> Map.add worker (Working (step, timeForStep step))
      legalSteps <- legalSteps |> Set.remove step
      remaining <- remaining |> Set.remove step

    // Pass one minute on all jobs. If done, release the worker and update the taken set.
    time <- time + 1
    for (worker, state) in workers |> Map.toList do
      match state with
      | NotWorking -> ()
      | Working (step, 1) ->
        workers <- workers |> Map.add worker NotWorking
        taken <- taken |> Set.add step
      | Working (step, time) -> workers <- workers |> Map.add worker (Working (step, time - 1))

  time

printfn "Part 2: %d" (calcP2 5 61)
