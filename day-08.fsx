type Node = { Metadata: int list; Children: Node list }

let rec parseNode data =
  match data with
  | childCount :: metadataCount :: data ->
    let children, data = parseMultipleNodes data childCount
    let metadata, data = data |> List.splitAt metadataCount
    { Metadata = metadata; Children = children }, data
  | _ -> raise <| System.Exception "malformed"

and parseMultipleNodes data count =
  match count with
  | 0 -> [], data
  | x ->
    let node, data = parseNode data
    let others, data = parseMultipleNodes data (x - 1)
    node :: others, data

let root, _ =
  System.IO.File.ReadAllText "day-08-input.txt"
  |> fun x -> x.Trim().Split ' '
  |> Array.toList
  |> List.map int
  |> parseNode

let rec metadataSum node =
  (node.Metadata |> List.sum) + (node.Children |> List.sumBy metadataSum)

printfn "Part 1: %d" (root |> metadataSum)

let rec metadataValue = function
  | { Metadata = m; Children = [] } -> List.sum m
  | node -> node.Metadata |> List.sumBy (metadataChildValue node)

and metadataChildValue node idx =
  if idx < 1 || idx > node.Children.Length then
    0
  else
    metadataValue node.Children.[idx - 1]

printfn "Part 2: %d" (root |> metadataValue)
