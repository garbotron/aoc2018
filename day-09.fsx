type Node = { Value: int64; mutable Prev: Node; mutable Next: Node }

let initNode() =
  let rec node = { Value = 0L; Prev = node; Next = node }
  node

let turn node value =
  if value % 23L = 0L then
    let removedNode = node.Prev.Prev.Prev.Prev.Prev.Prev.Prev
    removedNode.Prev.Next <- removedNode.Next
    removedNode.Next.Prev <- removedNode.Prev
    removedNode.Next, value + removedNode.Value
  else
    let newNode = { Value = value; Next = node.Next.Next; Prev = node.Next }
    node.Next.Next.Prev <- newNode
    node.Next.Next <- newNode
    newNode, 0L

let play playerCount maxValue =
  let foldTurn (node, scores) value =
    let playerIdx = (value - 1L) % playerCount
    let node, score = turn node value
    node, scores |> Map.add playerIdx (scores.[playerIdx] + score)

  let initScores = [0L..playerCount-1L] |> List.map (fun i -> int64 i, 0L) |> Map
  let _, finalScores = [1L..maxValue] |> List.fold foldTurn (initNode(), initScores)
  finalScores |> Map.toSeq |> Seq.map snd |> Seq.max

printfn "Part 1: %d" (play 476L 71431L)
printfn "Part 2: %d" (play 476L (71431L * 100L))
