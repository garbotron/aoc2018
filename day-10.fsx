open System.Text.RegularExpressions

type Star = { Position: int * int; Velocity: int * int }

let parseLine str =
  let m = Regex.Match(str, "position=< *(.*), *(.*)> velocity=< *(.*), *(.*)>")
  let grp (i: int) = m.Groups.[i].Value |> int
  { Position = grp 1, grp 2; Velocity = grp 3, grp 4 }

let input =
  System.IO.File.ReadAllLines "day-10-input.txt"
  |> Array.toList
  |> List.map parseLine

let add (x1, y1) (x2, y2) = x1 + x2, y1 + y2

let iter stars =
  stars |> List.map (fun s -> { s with Position = add s.Position s.Velocity })

let iters = seq {
  let mutable s = input
  while true do
    yield s
    s <- iter s }

let print stars =
  let pos = stars |> List.map (fun x -> x.Position)
  let minX = pos |> List.map fst |> List.min
  let maxX = pos |> List.map fst |> List.max
  let minY = pos |> List.map snd |> List.min
  let maxY = pos |> List.map snd |> List.max
  for y in [minY..maxY] do
    for x in [minX..maxX] do
      if stars |> List.exists (fun s -> s.Position = (x, y)) then
        printf "#"
      else
        printf "."
    printfn ""

let isMessage stars =
  // Guess at whether or not this is a valid message by how many adjacent stars there are.
  let set = stars |> List.map (fun x -> x.Position) |> Set
  let neighbors (x, y) = [x + 1, y; x - 1, y; x, y + 1; x, y - 1]
  let hasNeighbor star = neighbors star.Position |> List.exists (fun n -> set |> Set.contains n)
  (stars |> List.filter hasNeighbor |> List.length) > 300

printfn "Part 1:"
iters |> Seq.skipWhile (not << isMessage) |> Seq.head |> print

printfn "Part 2: %d" (iters |> Seq.indexed |> Seq.skipWhile (not << (snd >> isMessage)) |> Seq.head |> fst)
