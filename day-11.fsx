let sn = 4842
let allCoords = [1..300] |> List.collect (fun x -> [1..300] |> List.map (fun y -> x, y))

let power (x, y) =
  let rack = x + 10
  (((((rack * y) + sn) * rack) / 100) % 10) - 5

let buildBlockPowerMap map size =
  let power (x, y, n) =
    if n = 1 then
      let rack = x + 10
      let pow = (((((rack * y) + sn) * rack) / 100) % 10) - 5
      Some pow
    elif x + n - 1 > 300 || y + n - 1 > 300 then
      None // out of range
    else
      // To calculate the total power of a block of N size, calculate the size of 2 blocks size N-1, add the corners
      // and subtract the overlapping area. In other words, do the following:
      //   p(x, y, n) = p(x, y, n-1) + p (x+1, y+1, n-1) + p(x+n-1, y, 1) + p(x, y+n-1, 1) - p(x+1, y+1, n-2)
      let p x y n = map |> Map.find (x, y, n)
      let pow =
        p x y (n - 1)
        + p (x + 1) (y + 1) (n - 1)
        + p (x + n - 1) y 1
        + p x (y + n - 1) 1
        - (if n = 2 then 0 else p (x + 1) (y + 1) (n - 2))
      Some pow

  let folder map coord =
    match power coord with
    | None -> map
    | Some p -> map |> Map.add coord p

  allCoords |> List.map (fun (x, y) -> (x, y, size)) |> List.fold folder map

let blockPowerMap = [1..300] |> List.fold buildBlockPowerMap Map.empty

let bestBlock filter =
  blockPowerMap |> Map.toSeq |> Seq.filter (fst >> filter) |> Seq.maxBy snd |> fst

printfn "Part 1: %A" (bestBlock (fun (_, _, s) -> s = 3))
printfn "Part 2: %A" (bestBlock (fun _ -> true))
