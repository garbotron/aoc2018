open System.Text.RegularExpressions

type State = { Rules: Set<Set<int>>; State: Set<int> }

let foldInput state str =
  let str2set s = s |> Seq.indexed |> Seq.filter (snd >> ((=) '#')) |> Seq.map fst |> Set
  let m = Regex.Match(str, "initial state: (.*)")
  if m.Success then
    { state with State = m.Groups.[1].Value |> str2set }
  else
    let m = Regex.Match(str, @"([\.\#]{5}) => [\#]") // ignore "=> ."
    if m.Success then
      let rule = m.Groups.[1].Value |> str2set |> Set.map (fun x -> x - 2)
      { state with Rules = state.Rules |> Set.add rule }
    else
      state

let input =
  System.IO.File.ReadAllLines "day-12-input.txt"
  |> Array.fold foldInput { Rules = Set.empty; State = Set.empty }

let next state =
  let minIdx = state.State |> Set.minElement
  let maxIdx = state.State |> Set.maxElement
  let idxsToCheck = [minIdx - 2 .. maxIdx + 2]
  let grows idx =
    let cur = [idx - 2 .. idx + 2] |> List.filter (fun x -> state.State |> Set.contains x)
    let rule = cur |> List.map (fun x -> x - idx) |> Set
    state.Rules |> Set.contains rule
  { state with State = idxsToCheck |> List.filter grows |> Set }

let sum state = state.State |> Set.toSeq |> Seq.sum

let after20 = [1..20] |> List.fold (fun s _ -> next s) input
printfn "Part 1: %d" (after20 |> sum)

// For part 2, it is observed that eventually the number of elements in the set evens out at 22 and the total sum
// increases at a steady rate (22 per generation). We can figure out the final answer using simple math.
let after100 = [1..100] |> List.fold (fun s _ -> next s) input
let delta = (after100 |> next |> sum) - (after100 |> sum)
let total = (after100 |> sum |> int64) + ((50000000000L - 100L) * (int64 delta))
printfn "Part 2: %d" total
