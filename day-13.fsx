type Track = Intersection | Horizontal | Vertical | ULBRCorner | URBLCorner
type CartFacing = Up | Down | Left | Right
type Cart = { X: int; Y: int; Turns: int; Facing: CartFacing }
type CrashBehavior = RemoveCarts | ThrowException
exception Crash of int * int

let input =
  System.IO.File.ReadAllLines "day-13-input.txt"
  |> Array.toList
  |> List.indexed
  |> List.collect (fun (y, s) -> s |> Seq.toList |> List.indexed |> List.map (fun (x, c) -> (x, y), c))

let parseTrack (xy, c) =
  match c with
  | '-' | '<' | '>' -> Some (xy, Horizontal)
  | '|' | '^' | 'v' -> Some (xy, Vertical)
  | '/' -> Some (xy, ULBRCorner)
  | '\\' -> Some (xy, URBLCorner)
  | '+' -> Some (xy, Intersection)
  | _ -> None

let parseCart ((x, y), c) =
  match c with
  | '^' -> Some { X = x; Y = y; Turns = 0; Facing = Up }
  | 'v' -> Some { X = x; Y = y; Turns = 0; Facing = Down }
  | '<' -> Some { X = x; Y = y; Turns = 0; Facing = Left }
  | '>' -> Some { X = x; Y = y; Turns = 0; Facing = Right }
  | _ -> None

let tracks = input |> List.choose parseTrack |> Map
let carts = input |> List.choose parseCart

let turn cart =
  match tracks |> Map.find (cart.X, cart.Y), cart.Facing with
  | ULBRCorner, Left -> { cart with Facing = Down }
  | ULBRCorner, Right -> { cart with Facing = Up }
  | ULBRCorner, Up -> { cart with Facing = Right }
  | ULBRCorner, Down -> { cart with Facing = Left }
  | URBLCorner, Left -> { cart with Facing = Up }
  | URBLCorner, Right -> { cart with Facing = Down }
  | URBLCorner, Up -> { cart with Facing = Left }
  | URBLCorner, Down -> { cart with Facing = Right }
  | Intersection, facing ->
    let newFacing =
      match cart.Turns % 3, facing with
      | 1, f -> f // straight
      | 0, Up -> Left
      | 0, Down -> Right
      | 0, Left -> Down
      | 0, Right -> Up
      | 2, Up -> Right
      | 2, Down -> Left
      | 2, Left -> Up
      | 2, Right -> Down
      | _ -> raise <| System.Exception()
    { cart with Facing = newFacing; Turns = cart.Turns + 1 }
  | _ -> cart

let move cart =
  match cart.Facing with
  | Up -> { cart with Y = cart.Y - 1 }
  | Down -> { cart with Y = cart.Y + 1 }
  | Left -> { cart with X = cart.X - 1 }
  | Right -> { cart with X = cart.X + 1 }

let tick crashBehavior carts =
  let mutable unmoved = carts |> Set
  let mutable moved = Set.empty

  while not unmoved.IsEmpty do
    let cart = unmoved |> Seq.minBy (fun c -> c.Y, c.X)
    unmoved <- unmoved |> Set.remove cart
    let cart = cart |> turn |> move
    let collision = Set.union moved unmoved |> Seq.tryFind (fun c -> c.X = cart.X && c.Y = cart.Y)
    match collision with
    | None -> moved <- moved |> Set.add cart
    | Some _ when crashBehavior = ThrowException -> raise <| Crash (cart.X, cart.Y)
    | Some other ->
      unmoved <- unmoved |> Set.remove other
      moved <- moved |> Set.remove other

  moved |> Seq.toList

let ticks crashBehavior =
  carts |> Seq.unfold (fun c -> Some (c, tick crashBehavior c))

try
  ticks ThrowException |> Seq.last |> ignore
with
  | Crash (x, y) -> printfn "Part 1: %d,%d" x y

let lastCart = ticks RemoveCarts |> Seq.choose List.tryExactlyOne |> Seq.head
printfn "Part 2: %d,%d" lastCart.X lastCart.Y
