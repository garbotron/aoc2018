#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

#define MAX_RECIPE_COUNT 100000000

static char recipe_buf[MAX_RECIPE_COUNT];
static int recipe_count;
static int elf1;
static int elf2;

static void init(void) {
  recipe_count = 2;
  recipe_buf[0] = 3;
  recipe_buf[1] = 7;
  elf1 = 0;
  elf2 = 1;
}

static void add_recipe(char recipe) {
  assert(recipe_count < MAX_RECIPE_COUNT);
  recipe_buf[recipe_count] = recipe;
  recipe_count++;
}

static int move_elf(int elf) {
  int move_by = recipe_buf[elf] + 1;
  return (elf + move_by) % recipe_count;
}

static void step(void) {
  char sum = recipe_buf[elf1] + recipe_buf[elf2];
  if (sum >= 10) {
    add_recipe(sum / 10);
  }
  add_recipe(sum % 10);
  elf1 = move_elf(elf1);
  elf2 = move_elf(elf2);
}

static void part1(void) {
  int skip = 505961;
  int count = 10;

  init();
  while (recipe_count < skip + count) {
    step();
  }

  printf("Part 1: ");
  for (int i = 0; i < 10; i++) {
    printf("%d", recipe_buf[skip + i]);
  }
  printf("\n");
}

static bool is_match_part2(int end) {
  return (end >= 6
    && recipe_buf[end - 6] == 5
    && recipe_buf[end - 5] == 0
    && recipe_buf[end - 4] == 5
    && recipe_buf[end - 3] == 9
    && recipe_buf[end - 2] == 6
    && recipe_buf[end - 1] == 1);
}

static int find_recipes_before_part2_match(void) {
  init();
  while (true) {
    if (is_match_part2(recipe_count)) {
      return recipe_count - 6;
    }
    if (is_match_part2(recipe_count - 1)) {
      return recipe_count - 7;
    }
    step();
  }
}

static void part2(void) {
  printf("Part 2: %d\n", find_recipes_before_part2_match());
}

void main(void) {
  part1();
  part2();
}
