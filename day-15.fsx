type Side = Elf | Goblin
type Unit = { Side: Side; Position: int * int; HP: int; AttackPower: int }
type State = { Walls: Set<int * int>; Units: Map<int * int, Unit>; Active: bool }

let defaultUnit side pos = { Side = side; Position = pos; HP = 200; AttackPower = 3 }

let foldInput state (y, str) =
  let foldChar state (x, ch) =
    match ch with
    | '#' -> { state with Walls = state.Walls |> Set.add (x, y) }
    | 'G' -> { state with Units = state.Units |> Map.add (x, y) (defaultUnit Goblin (x, y)) }
    | 'E' -> { state with Units = state.Units |> Map.add (x, y) (defaultUnit Elf (x, y)) }
    | _ -> state
  str |> Seq.indexed |> Seq.fold foldChar state

let initialState =
  System.IO.File.ReadAllLines "day-15-input.txt"
  |> Array.indexed
  |> Array.fold foldInput { Walls = Set.empty; Units = Map.empty; Active = true }

let initialStateWithAttackPower attackPower =
  let setAttackPower _ unit =
    match unit.Side with
    | Elf -> { unit with AttackPower = attackPower }
    | Goblin -> unit
  { initialState with Units = initialState.Units |> Map.map setAttackPower }

let infinity = System.Int32.MaxValue
let neighbors (x, y) = [x, y - 1; x - 1, y; x + 1, y; x, y + 1]
let dist (x1, y1) (x2, y2) = abs (x1 - x2) + abs (y1 - y2)
let isOpen state pos = not (state.Walls |> Set.contains pos || state.Units |> Map.containsKey pos)

type AStarState = {
  OpenSet: Set<int * int>
  GScore: Map<int * int, int>
  FScore: Map<int * int, int>
  Found: bool }

let aStar (src: int * int) (dest: int * int) (globalState: State): int option =
  let h pos = dist dest pos

  let initialState = {
    OpenSet = Set [src]
    GScore = Map [src, 0]
    FScore = Map [src, h src]
    Found = false }

  let rec explore state =
    if state.OpenSet |> Set.isEmpty then
      state
    else
      let current = state.OpenSet |> Seq.minBy (fun x -> state.FScore |> Map.find x)
      if current = dest then
        { state with Found = true }
      else
        let state = { state with OpenSet = state.OpenSet |> Set.remove current }
        current
          |> neighbors
          |> List.filter (isOpen globalState)
          |> List.fold (exploreNeighbor current) state
          |> explore

  and exploreNeighbor current state neighbor =
    let tentativeGScore = (state.GScore |> Map.find current) + 1
    if tentativeGScore < (state.GScore |> Map.tryFind neighbor |> Option.defaultValue infinity) then
      { state with
          OpenSet = state.OpenSet |> Set.add neighbor
          GScore = state.GScore |> Map.add neighbor tentativeGScore
          FScore = state.FScore |> Map.add neighbor (tentativeGScore + h neighbor) }
    else
      state

  match initialState |> explore with
  | { Found = false } -> None
  | { Found = true; GScore = gScore } -> Some (gScore |> Map.find dest)

let stepToShortestPath (unit: Unit) (targets: Map<int * int, Unit>) (state: State): Option<int * int> =
  let openNeighbors target = target.Position |> neighbors |> List.filter (isOpen state)
  let possibleFirstSteps = unit |> openNeighbors
  let possibleLastSteps = targets |> Map.toList |> List.map snd |> List.collect openNeighbors

  if possibleFirstSteps.IsEmpty || possibleLastSteps.IsEmpty then
    None
  else
    // Run A* from the unit to each potential last step to find the closest.
    let lastStep =
      possibleLastSteps
      |> List.map (fun x -> x, state |> aStar unit.Position x)
      |> List.filter (snd >> Option.isSome)
      |> List.sortBy (fun ((x, y), l) -> l, y, x)
      |> List.tryHead
      |> Option.map fst

    match lastStep with
    | None -> None
    | Some lastStep ->
      // Once we know the last step, find the first step that provides the shortest path to it.
      possibleFirstSteps
        |> List.map (fun x -> x, state |> aStar x lastStep)
        |> List.filter (snd >> Option.isSome)
        |> List.sortBy (fun ((x, y), l) -> l, y, x)
        |> List.tryHead
        |> Option.map fst

let combat unit targets state =
  let rev (x, y) = y, x
  let target = targets |> Map.toList |> List.map snd |> List.minBy (fun x -> x.HP, rev x.Position)
  let newHP = target.HP - unit.AttackPower
  if newHP <= 0 then
    { state with Units = state.Units |> Map.remove target.Position }
  else
    { state with Units = state.Units |> Map.add target.Position { target with HP = newHP } }

let moveUnit unit pos state =
  { state with Units = state.Units |> Map.remove unit.Position |> Map.add pos { unit with Position = pos } }

let turn unit state =
  let targets = state.Units |> Map.filter (fun _ x -> x.Side <> unit.Side)
  if targets |> Map.isEmpty then
    { state with Active = false }
  else
    let inRangeTargets = targets |> Map.filter (fun _ x -> dist unit.Position x.Position = 1)
    if inRangeTargets |> Map.isEmpty then
      match stepToShortestPath unit targets state with
      | None -> state
      | Some newPos ->
        let state = state |> moveUnit unit newPos
        let inRangeTargets = targets |> Map.filter (fun _ x -> dist newPos x.Position = 1)
        if inRangeTargets |> Map.isEmpty then
          state
        else
          state |> combat unit inRangeTargets
    else
      state |> combat unit inRangeTargets

let round state =
  let foldTurn state pos =
    if state.Active then
      match state.Units |> Map.tryFind pos with
      | None -> state
      | Some unit -> state |> turn unit
    else
      state
  state.Units |> Map.toList |> List.map fst |> List.sortBy (fun (x, y) -> y, x) |> List.fold foldTurn state

let outcome (rounds, state) =
  (rounds - 1) * (state.Units |> Map.toSeq |> Seq.sumBy (fun (_, x) -> x.HP))

let runUntilInactive attackPower =
  Seq.unfold (fun state -> Some (state, state |> round)) (initialStateWithAttackPower attackPower)
  |> Seq.indexed
  |> Seq.skipWhile (fun (_, x) -> x.Active)
  |> Seq.head

let rec findOutcomeWithNoDeadElves min max =
  let elfCount s = s.Units |> Map.filter (fun _ x -> x.Side = Elf) |> Map.count
  if min = max then
    runUntilInactive min
  else
    // Try the middle of the range (binary search).
    let mid = (min + max) / 2
    let _, state = runUntilInactive mid
    if elfCount state = elfCount initialState then
      // No elves killed. Update upper bound (inclusive).
      findOutcomeWithNoDeadElves min mid
    else
      // At least one elf was killed. Update lower bound (exclusive).
      findOutcomeWithNoDeadElves (mid + 1) max

printfn "Part 1: %d" (runUntilInactive 3 |> outcome)
printfn "Part 2: %d" (findOutcomeWithNoDeadElves 3 200 |> outcome)
