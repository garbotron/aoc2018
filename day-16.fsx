type Cpu =
  { Regs: Map<int, int> }
  member this.Get reg = this.Regs |> Map.tryFind reg |> Option.defaultValue 0
  member this.Set reg value = { this with Regs = this.Regs |> Map.add reg value }

type Op = int -> int -> int -> Cpu -> Cpu
type Instruction = { Opcode: int; A: int; B: int; C: int }
type SampleRecord = { Before: Cpu; After: Cpu; Instruction: Instruction }

let ops: Op list = [
  fun a b c cpu -> cpu.Set c (cpu.Get a + cpu.Get b)
  fun a b c cpu -> cpu.Set c (cpu.Get a + b)
  fun a b c cpu -> cpu.Set c (cpu.Get a * cpu.Get b)
  fun a b c cpu -> cpu.Set c (cpu.Get a * b)
  fun a b c cpu -> cpu.Set c (cpu.Get a &&& cpu.Get b)
  fun a b c cpu -> cpu.Set c (cpu.Get a &&& b)
  fun a b c cpu -> cpu.Set c (cpu.Get a ||| cpu.Get b)
  fun a b c cpu -> cpu.Set c (cpu.Get a ||| b)
  fun a _ c cpu -> cpu.Set c (cpu.Get a)
  fun a _ c cpu -> cpu.Set c a
  fun a b c cpu -> cpu.Set c (if a > cpu.Get b then 1 else 0)
  fun a b c cpu -> cpu.Set c (if cpu.Get a > b then 1 else 0)
  fun a b c cpu -> cpu.Set c (if cpu.Get a > cpu.Get b then 1 else 0)
  fun a b c cpu -> cpu.Set c (if a = cpu.Get b then 1 else 0)
  fun a b c cpu -> cpu.Set c (if cpu.Get a = b then 1 else 0)
  fun a b c cpu -> cpu.Set c (if cpu.Get a = cpu.Get b then 1 else 0)
]

let parseInstruction (line: string) =
  let nums = line.Split(" ") |> Array.map int
  { Opcode = nums.[0]; A = nums.[1]; B = nums.[2]; C = nums.[3] }

let parseSampleRecord (lines: string list) =
  let cpuFromLine i =
    lines.[i].Substring(9, 10).Split(", ")
    |> Array.map int
    |> Array.indexed
    |> fun x -> { Regs = Map x }
  { Before = cpuFromLine 0
    After = cpuFromLine 2
    Instruction = parseInstruction lines.[1] }

let sampleRecords =
  System.IO.File.ReadAllLines "day-16-input-a.txt"
  |> Array.toList
  |> List.chunkBySize 4
  |> List.map parseSampleRecord

let testProgram =
  System.IO.File.ReadAllLines "day-16-input-b.txt"
  |> Array.toList
  |> List.map parseInstruction

let recordBehavesLike record op =
  record.After = (record.Before |> op record.Instruction.A record.Instruction.B record.Instruction.C)

let opCountForRecord record = ops |> List.filter (recordBehavesLike record) |> List.length

printfn "Part 1: %d" (sampleRecords |> List.map opCountForRecord |> List.filter (fun x -> x >= 3) |> List.length)

let possibleOpcodes op =
  sampleRecords
  |> List.filter (fun r -> recordBehavesLike r op)
  |> List.map (fun r -> r.Instruction.Opcode)
  |> List.distinct

let rec reduceOpcodes map =
  // Find all opcodes that are "solved" (only one number remains).
  let solved, unsolved = map |> List.partition (snd >> fun x -> x |> List.length = 1)
  if unsolved.IsEmpty then
    map
  else
    let unsolved =
      unsolved
      |> List.map (fun (op, codes) -> op, codes |> List.except (solved |> List.collect snd))
    solved @ unsolved |> reduceOpcodes

let opcodeMap =
  ops
  |> List.map (fun x -> x, possibleOpcodes x)
  |> reduceOpcodes
  |> List.map (fun (op, codes) -> codes.[0], op)
  |> Map

let runInstruction cpu instr =
  let op = opcodeMap |> Map.find instr.Opcode
  cpu |> op instr.A instr.B instr.C

let result = testProgram |> List.fold runInstruction { Regs = Map.empty }
printfn "Part 2: %d" (result.Get 0)
