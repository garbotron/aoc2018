type State = {
  Walls: Set<int * int>
  Wet: Set<int * int>
  MinY: int
  MaxY: int }

let foldInputLine (clay: Set<int * int>) (line: string) =
  let parseRange (str: string) = str.Split("..") |> fun x -> int x.[0], int x.[1]

  let terms = line.Split(", ")
  if terms.[0].StartsWith("x") then
    // x is constant, y is a range
    let x = terms.[0].Substring(2) |> int
    let yMin, yMax = terms.[1].Substring(2) |> parseRange
    [yMin..yMax] |> List.map (fun y -> x, y) |> Set |> Set.union clay
  else
    // y is constant, y is a range
    let y = terms.[0].Substring(2) |> int
    let xMin, xMax = terms.[1].Substring(2) |> parseRange
    [xMin..xMax] |> List.map (fun x -> x, y) |> Set |> Set.union clay

let input =
  System.IO.File.ReadAllLines "day-17-input.txt"
  |> Array.toList
  |> List.fold foldInputLine Set.empty

let initialState = {
  Walls = input
  Wet = Set.empty
  MinY = input |> Seq.map snd |> Seq.min
  MaxY = input |> Seq.map snd |> Seq.max }

let hSlice y xMin xMax = [xMin..xMax] |> List.map (fun x -> x, y) |> Set
let vSlice x yMin yMax = [yMin..yMax] |> List.map (fun y -> x, y) |> Set

let rec drip (spoutX, spoutY) state =
  // Fall downward until you hit either clay, settled water, or the bottom of the area.
  let hit = seq { spoutY .. state.MaxY } |> Seq.tryFind (fun y -> state.Walls |> Set.contains (spoutX, y))
  match hit with
  | None -> { state with Wet = state.Wet |> Set.union (vSlice spoutX spoutY state.MaxY) }
  | Some y ->
    // We hit something. Scan to the left and right until we drop / hit a wall.
    let downward = vSlice spoutX spoutY (y - 2)
    if state.Wet |> Set.contains (spoutX, y - 1) then
       // This spot is already wet, meaning we've already been here.
      { state with Wet = state.Wet |> Set.union downward }
    else
      { state with Wet = state.Wet |> Set.union downward } |> spread (spoutX, y - 1)

and spread (x, y) state =
  let state, left = state |> scanX y x -1
  let state, right = state |> scanX y x 1
  match left, right with
  | Some l, Some r ->
    // Settle the water between the left and right walls.
    let state = { state with Walls = state.Walls |> Set.union (hSlice y (l + 1) (r - 1)) }
    // Continue this process one tile upwards.
    state |> spread (x, y - 1)
  | _ -> state // we dripped off one side or other, no need to pile upwards

and scanX y x dx state =
  if state.Walls |> Set.contains (x, y) then
    state, Some x // hit a wall, return the wall position to the caller
  elif not (state.Walls |> Set.contains (x, y + 1)) then
    state |> drip (x, y), None // dropped of an edge, continue simulation
  else
    { state with Wet = state.Wet |> Set.add (x, y) } |> scanX y (x + dx) dx // continue

let result = initialState |> drip (500, initialState.MinY)
printfn "Part 1: %d" result.Wet.Count
printfn "Part 2: %d" (result.Wet |> Set.intersect result.Walls |> Set.count)
