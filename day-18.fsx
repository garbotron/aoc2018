type Tile = Open | Wooded | Lumberyard

let foldInputLine tiles (y, line) =
  let chToTile = function
    | '.' -> Open
    | '|' -> Wooded
    | '#' -> Lumberyard
    | _ -> raise <| System.Exception()
  let foldChar tiles (x, ch) = tiles |> Map.add (x, y) (ch |> chToTile)
  line |> Seq.indexed |> Seq.fold foldChar tiles

let input =
  System.IO.File.ReadAllLines "day-18-input.txt"
  |> Seq.indexed
  |> Seq.fold foldInputLine Map.empty

let neighbors (x, y) = [
  x - 1, y - 1
  x - 1, y
  x - 1, y + 1
  x, y - 1
  x, y + 1
  x + 1, y - 1
  x + 1, y
  x + 1, y + 1 ]

let next coord map =
  let neighbors = neighbors coord |> List.choose (fun x -> map |> Map.tryFind x)
  let neighbors tile = neighbors |> List.filter ((=) tile) |> List.length
  match map |> Map.find coord with
  | Open when (neighbors Wooded >= 3) -> Wooded
  | Wooded when (neighbors Lumberyard >= 3) -> Lumberyard
  | Lumberyard when (neighbors Lumberyard = 0 || neighbors Wooded = 0) -> Open
  | x -> x

let minute map = map |> Map.map (fun k _ -> map |> next k)
let minutes = Seq.unfold (fun s -> Some (s, minute s))

let resourceValue map =
  let resourceCount tile = map |> Map.filter (fun _ v -> v = tile) |> Map.count
  (resourceCount Wooded) * (resourceCount Lumberyard)

printfn "Part 1: %d" (input |> minutes |> Seq.skip 10 |> Seq.head |> resourceValue)

let findCycle map =
  let rec go results cur i =
    match results |> Map.tryFind cur with
    | Some j -> j, (i - j)
    | None -> go (results |> Map.add cur i) (cur |> minute) (i + 1)
  go Map.empty map 0

let skipTo time map =
  let cycleStart, cycleLen = map |> findCycle
  let repsToRun = cycleStart + ((time - cycleStart) % cycleLen)
  map |> minutes |> Seq.skip repsToRun |> Seq.head

printfn "Part 2: %d" (input |> skipTo 1000000000 |> resourceValue)
