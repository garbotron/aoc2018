type Op = int64 -> int64 -> int64 -> Cpu -> Cpu

and Instruction = { Name: string; Func: Op; A: int64; B: int64; C: int64 }

and Cpu =
  { Regs: Map<int64, int64>
    Instructions: Map<int64, Instruction>
    IPReg: int64;
    Running: bool }
  member this.Get reg = this.Regs |> Map.tryFind reg |> Option.defaultValue 0L
  member this.Set reg value = { this with Regs = this.Regs |> Map.add reg value }

let opFuncs: Map<string, Op> = Map <| [
  "addr", fun a b c cpu -> cpu.Set c (cpu.Get a + cpu.Get b)
  "addi", fun a b c cpu -> cpu.Set c (cpu.Get a + b)
  "mulr", fun a b c cpu -> cpu.Set c (cpu.Get a * cpu.Get b)
  "muli", fun a b c cpu -> cpu.Set c (cpu.Get a * b)
  "banr", fun a b c cpu -> cpu.Set c (cpu.Get a &&& cpu.Get b)
  "bani", fun a b c cpu -> cpu.Set c (cpu.Get a &&& b)
  "borr", fun a b c cpu -> cpu.Set c (cpu.Get a ||| cpu.Get b)
  "bori", fun a b c cpu -> cpu.Set c (cpu.Get a ||| b)
  "setr", fun a _ c cpu -> cpu.Set c (cpu.Get a)
  "seti", fun a _ c cpu -> cpu.Set c a
  "gtir", fun a b c cpu -> cpu.Set c (if a > cpu.Get b then 1L else 0L)
  "gtri", fun a b c cpu -> cpu.Set c (if cpu.Get a > b then 1L else 0L)
  "gtrr", fun a b c cpu -> cpu.Set c (if cpu.Get a > cpu.Get b then 1L else 0L)
  "eqir", fun a b c cpu -> cpu.Set c (if a = cpu.Get b then 1L else 0L)
  "eqri", fun a b c cpu -> cpu.Set c (if cpu.Get a = b then 1L else 0L)
  "eqrr", fun a b c cpu -> cpu.Set c (if cpu.Get a = cpu.Get b then 1L else 0L) ]

let foldInputLine (cpu: Cpu) (str: string) =
  let words = str.Split ' '
  if words.[0] = "#ip" then
    { cpu with IPReg = int64 words.[1] }
  else
    let num i = if i < words.Length then int64 words.[i] else 0L
    let instr = { Name = words.[0]; Func = opFuncs |> Map.find words.[0]; A = num 1; B = num 2; C = num 3 }
    { cpu with Instructions = cpu.Instructions |> Map.add (int64 cpu.Instructions.Count) instr }

let initialCpu =
  System.IO.File.ReadAllLines "day-19-input.txt"
  |> Seq.fold foldInputLine { Regs = Map.empty; Instructions = Map.empty; IPReg = 0L; Running = true }

let run (cpu: Cpu) =
  match cpu.Instructions |> Map.tryFind (cpu.Get cpu.IPReg) with
  | None -> { cpu with Running = false }
  | Some i -> cpu |> i.Func i.A i.B i.C |> fun x -> x.Set x.IPReg ((x.Get x.IPReg) + 1L)

let rec runUntilHalt (cpu: Cpu) =
  match cpu with
  | { Running = false } -> cpu
  | { Running = true } -> cpu |> run |> runUntilHalt

printfn "Part 1: %d" (initialCpu |> runUntilHalt |> fun x -> Map.find 0L x.Regs)

// For part 2, I had to inspect the code and rework it as pseudocode to figure out what it did.
// For this I labeled the registers as a-f (reg 0-reg 5).
// First, the code computes b. It arrives at a different total depending on the value of a (part 1 vs 2).
// For part 2, b is 10551311.
// Once b is calculated, the code does the following:
// for (c = 1; c <= b; c++) {
//     for (f = 1; f <= b; f++) {
//         if (c * f == b) {
//             a += c
//         }
//     }
// }
// This code sums all of the factors of b. Using Wolfram Alpha I calculated the factors of b then add them here.
printfn "Part 2: %d" (1 + 431 + 24481 + 10551311)
