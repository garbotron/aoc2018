#r "nuget: FParsec"
open FParsec

type Dir = North | South | East | West

let mutable curTokenID = 0
let nextTokenID() =
  curTokenID <- curTokenID + 1
  curTokenID

type TokenContent = DirToken of Dir | ChoiceToken of Token list list
and Token =
  { ID: int; Content: TokenContent }
  static member Dir dir = { ID = nextTokenID(); Content = DirToken dir }
  static member Choice choices = { ID = nextTokenID(); Content = ChoiceToken choices }

let parseToken, parseTokenRef = createParserForwardedToRef<Token, unit>()
parseTokenRef := choice [
  pchar 'N' >>% North |>> Token.Dir
  pchar 'S' >>% South |>> Token.Dir
  pchar 'E' >>% East |>> Token.Dir
  pchar 'W' >>% West |>> Token.Dir
  pchar '(' >>. ((many parseToken |> sepBy <| skipChar '|') |>> Token.Choice) .>> pchar ')'
]

let input =
  System.IO.File.ReadAllText "day-20-input.txt"
  |> fun x -> x.Trim [| '\n'; '^'; '$' |]
  |> run (many parseToken)
  |> function
     | Success (r, _, _) -> r
     | _ -> raise <| System.Exception()

let add (x1, y1) (x2, y2) = (x1 + x2), (y1 + y2)

let explore tokens =
  let mutable doors = Map.empty // map of each coordinate to all of it's connected neighbors
  let mutable cache = Set.empty // set of all visited (position * node ID) pairs

  let travel src offset =
    let dest = add src offset
    let srcConns = doors |> Map.tryFind src |> Option.defaultValue Set.empty
    let destConns = doors |> Map.tryFind dest |> Option.defaultValue Set.empty
    doors <- doors |> Map.add src (srcConns |> Set.add dest)
    doors <- doors |> Map.add dest (destConns |> Set.add src)
    dest

  // Process a token from the given position and return a list of all points where we could end up afterward.
  let rec processToken pos token =
    if cache |> Set.contains (pos, token.ID) then
      [] // already been here
    else
      cache <- cache |> Set.add (pos, token.ID)
      match token.Content with
      | DirToken North -> [travel pos (0, -1)]
      | DirToken South -> [travel pos (0, 1)]
      | DirToken East -> [travel pos (1, 0)]
      | DirToken West -> [travel pos (-1, 0)]
      | ChoiceToken choices -> choices |> List.collect (processTokens pos)

  and processTokens pos tokens =
    match tokens with
    | [] -> [pos]
    | head :: tail -> processToken pos head |> List.collect (fun x -> processTokens x tail)

  ignore <| processTokens (0, 0) tokens
  doors

let createShortestPathMap map =
  let mutable shortestPaths = Map.empty
  let rec visit pos cur =
    let prev = shortestPaths |> Map.tryFind pos |> Option.defaultValue System.Int32.MaxValue
    if cur < prev then
       shortestPaths <- shortestPaths |> Map.add pos cur
       map |> Map.find pos |> Seq.iter (fun x -> visit x (cur + 1))
  visit (0, 0) 0
  shortestPaths

let furthestRoomDist shortestPaths =
  shortestPaths |> Map.toSeq |> Seq.map snd |> Seq.max

let farAwayRoomCount shortestPaths =
  shortestPaths |> Map.toSeq |> Seq.filter (fun (_, x) -> x >= 1000) |> Seq.length

printfn "Part 1: %d" (input |> explore |> createShortestPathMap |> furthestRoomDist)
printfn "Part 2: %d" (input |> explore |> createShortestPathMap |> farAwayRoomCount)
