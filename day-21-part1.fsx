type Op = int -> int -> int -> Cpu -> Cpu

and Instruction = { Name: string; Func: Op; A: int; B: int; C: int }

and Cpu =
  { Regs: Map<int, int>
    Instructions: Map<int, Instruction>
    IPReg: int;
    Running: bool }
  member this.Get reg = this.Regs |> Map.tryFind reg |> Option.defaultValue 0
  member this.Set reg value = { this with Regs = this.Regs |> Map.add reg value }

let opFuncs: Map<string, Op> = Map <| [
  "addr", fun a b c cpu -> cpu.Set c (cpu.Get a + cpu.Get b)
  "addi", fun a b c cpu -> cpu.Set c (cpu.Get a + b)
  "mulr", fun a b c cpu -> cpu.Set c (cpu.Get a * cpu.Get b)
  "muli", fun a b c cpu -> cpu.Set c (cpu.Get a * b)
  "banr", fun a b c cpu -> cpu.Set c (cpu.Get a &&& cpu.Get b)
  "bani", fun a b c cpu -> cpu.Set c (cpu.Get a &&& b)
  "borr", fun a b c cpu -> cpu.Set c (cpu.Get a ||| cpu.Get b)
  "bori", fun a b c cpu -> cpu.Set c (cpu.Get a ||| b)
  "setr", fun a _ c cpu -> cpu.Set c (cpu.Get a)
  "seti", fun a _ c cpu -> cpu.Set c a
  "gtir", fun a b c cpu -> cpu.Set c (if a > cpu.Get b then 1 else 0)
  "gtri", fun a b c cpu -> cpu.Set c (if cpu.Get a > b then 1 else 0)
  "gtrr", fun a b c cpu -> cpu.Set c (if cpu.Get a > cpu.Get b then 1 else 0)
  "eqir", fun a b c cpu -> cpu.Set c (if a = cpu.Get b then 1 else 0)
  "eqri", fun a b c cpu -> cpu.Set c (if cpu.Get a = b then 1 else 0)
  "eqrr", fun a b c cpu -> cpu.Set c (if cpu.Get a = cpu.Get b then 1 else 0) ]

let foldInputLine (cpu: Cpu) (str: string) =
  let words = str.Split ' '
  if words.[0] = "#ip" then
    { cpu with IPReg = int words.[1] }
  else
    let num i = if i < words.Length then int words.[i] else 0
    let instr = { Name = words.[0]; Func = opFuncs |> Map.find words.[0]; A = num 1; B = num 2; C = num 3 }
    { cpu with Instructions = cpu.Instructions |> Map.add (cpu.Instructions.Count) instr }

let initialCpu =
  System.IO.File.ReadAllLines "day-21-input.txt"
  |> Seq.fold foldInputLine { Regs = Map.empty; Instructions = Map.empty; IPReg = 0; Running = true }

let run cpu =
  match cpu.Instructions |> Map.tryFind (cpu.Get cpu.IPReg) with
  | None -> { cpu with Running = false }
  | Some i -> cpu |> i.Func i.A i.B i.C |> fun x -> x.Set x.IPReg ((x.Get x.IPReg) + 1)

// Register 0 is only accessed once, near the end of the program. If it is equal to the value in register 2, we will
// jump out of bounds and the program will halt. We capture the value of register 2 at the moment of this check.
let rec findAnswer cpu =
  let instr = cpu.Instructions |> Map.find (cpu.Get cpu.IPReg)
  if instr.Name = "eqrr" && instr.A = 2 && instr.B = 0 && instr.C = 3 then
    cpu.Get 2
  else
    cpu |> run |> findAnswer

printfn "Part 1: %d" (initialCpu |> findAnswer)
