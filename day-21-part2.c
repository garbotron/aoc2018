// Machine code for day 21 translated into C.
// Used to calculate the answer for part 2: keep caluclating potential answers until you run into the last
// non-duplicate result (any further results will execute fewer instructions).

#include <stdio.h>

static int a, b, c, d, f;
static int answers[0x1000000];
static int last_answer;

static void run(void) {
  c = 0;
line_6:
  f = c | 0x10000;
  c = 0x4FDFAC;
line_8:
  c = (((c + (f & 0xFF)) & 0xFFFFFF) * 0x1016B) & 0xFFFFFF;
  if (0x100 > f) {
    goto line_28;
  }
  d = 0;
line_18:
  b = d + 1;
  b = b * 0x100;
  if (b > f) {
    goto line_26;
  }
  d++;
  goto line_18;
line_26:
  f = d;
  goto line_8;
line_28:
  if (c == a) {
    printf("HALT\n");
    return;
  }

  // Custom code added to check for the first duplicate answer.
  if (answers[c]) {
    printf("Part 2: %d\n", last_answer);
    return;
  } else {
    answers[c] = 1;
    last_answer = c;
  }

  goto line_6;
}

void main(void) {
  run();
}
