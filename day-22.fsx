type Tool = Neither | Torch | ClimbingGear
type SearchCoord = { X: int; Y: int; Tool: Tool }

let createRegionTypeMap depth targetX targetY maxX maxY =
  let geoToErosion x = (x + depth) % 20183
  let mutable eroisionLevels = Map.empty

  // (0, 0) and target erosion levels are 0.
  eroisionLevels <- eroisionLevels |> Map.add (0, 0) 0
  eroisionLevels <- eroisionLevels |> Map.add (targetX, targetY) 0

  // Calculate erosion levels for all coords where Y=0.
  for x in [1..maxX] do
    eroisionLevels <- eroisionLevels |> Map.add (x, 0) (x * 16807 |> geoToErosion)

  // Calculate erosion levels for all coords where X=0.
  for y in [1..maxY] do
    eroisionLevels <- eroisionLevels |> Map.add (0, y) (y * 48271 |> geoToErosion)

  // Recursively calculate the rest of the erosion levels.
  let rec calcErosion x y =
    match eroisionLevels |> Map.tryFind (x, y) with
    | Some value -> value
    | None ->
      let value = (calcErosion x (y - 1)) * (calcErosion (x - 1) y) |> geoToErosion
      eroisionLevels <- eroisionLevels |> Map.add (x, y) value
      value

  ignore <| calcErosion (maxX - 1) maxY
  ignore <| calcErosion maxX (maxY - 1)

  eroisionLevels |> Map.map (fun _ v -> v % 3)

let transitions map ((x, y), tool) =
  // We can always switch tools...
  let otherTool =
    match map |> Map.find (x, y), tool with
    | 0, Torch -> ClimbingGear
    | 0, ClimbingGear -> Torch
    | 1, Neither -> ClimbingGear
    | 1, ClimbingGear -> Neither
    | 2, Neither -> Torch
    | 2, Torch -> Neither
    | _ -> raise <| System.Exception()

  // Or we can go any direction that permits our current tool.
  let canGoTo = function
  | Some 0 -> (tool = Torch || tool = ClimbingGear)
  | Some 1 -> (tool = Neither || tool = ClimbingGear)
  | Some 2 -> (tool = Neither || tool = Torch)
  | None -> false
  | _ -> raise <| System.Exception()

  let neighbors =
    [x + 1, y; x, y + 1; x - 1, y; x, y - 1]
    |> List.filter (fun x -> map |> Map.tryFind x |> canGoTo)
    |> List.map (fun x -> x, tool, 1)
  neighbors @ [(x, y), otherTool, 7]

type AStarState = {
  OpenSet: Set<(int * int) * Tool>
  GScore: Map<(int * int) * Tool, int>
  FScore: Map<(int * int) * Tool, int>
  Found: bool }

let infinity = System.Int32.MaxValue
let dist (x1, y1) (x2, y2) = abs (x1 - x2) + abs (y1 - y2)

let aStar (src: (int * int) * Tool) (dest: (int * int) * Tool) (map: Map<int * int, int>): int option =
  let h (pos, tool) =
    let destPos, destTool = dest
    dist pos destPos + (if tool = destTool then 0 else 7)

  let initialState = {
    OpenSet = Set [src]
    GScore = Map [src, 0]
    FScore = Map [src, h src]
    Found = false }

  let rec explore state =
    if state.OpenSet |> Set.isEmpty then
      state
    else
      let current = state.OpenSet |> Seq.minBy (fun x -> state.FScore |> Map.find x)
      if current = dest then
        { state with Found = true }
      else
        let state = { state with OpenSet = state.OpenSet |> Set.remove current }
        current
          |> transitions map
          |> List.fold (exploreNeighbor current) state
          |> explore

  and exploreNeighbor current state ((x, y), tool, cost) =
    let tentativeGScore = (state.GScore |> Map.find current) + cost
    if tentativeGScore < (state.GScore |> Map.tryFind ((x, y), tool) |> Option.defaultValue infinity) then
      { state with
          OpenSet = state.OpenSet |> Set.add ((x, y), tool)
          GScore = state.GScore |> Map.add ((x, y), tool) tentativeGScore
          FScore = state.FScore |> Map.add ((x, y), tool) (tentativeGScore + h ((x, y), tool)) }
    else
      state

  match initialState |> explore with
  | { Found = false } -> None
  | { Found = true; GScore = gScore } -> Some (gScore |> Map.find dest)

let shortestPathToTarget target map =
  map |> aStar ((0, 0), Torch) (target, Torch) |> Option.get

printfn "Part 1: %d" (createRegionTypeMap 9171 7 721 7 721 |> Map.toSeq |> Seq.sumBy snd)

// For part 2, create a map that's much bigger in case we need to go off the edges.
let bigMap = createRegionTypeMap 9171 7 721 200 1500
printfn "Part 2: %d" (bigMap |> shortestPathToTarget (7, 721))
