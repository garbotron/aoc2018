open System.Text.RegularExpressions

type Nanobot = { Pos: int * int * int; Radius: int }

let parseNanobot (str: string) =
  let m = Regex.Match(str, "pos=<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>, r=([0-9]+)")
  let num (i: int) = m.Groups.[i].Value |> int
  { Pos = num 1, num 2, num 3; Radius = num 4 }

let nanobots =
  System.IO.File.ReadAllLines "day-23-input.txt"
  |> Array.toList
  |> List.map parseNanobot

let dist (x1, y1, z1) (x2, y2, z2) =
  abs (x1 - x2) + abs (y1 - y2) + abs (z1 - z2)

let inRange bot point =
  dist bot.Pos point <= bot.Radius

let strongest = nanobots |> List.maxBy (fun x -> x.Radius)
printfn "Part 1: %d" (nanobots |> List.map (fun x -> x.Pos) |> List.filter (inRange strongest) |> List.length)

// Find the center point of where 2 nanobots' shperes of influence intersect.
// This will effectively identify all intersected regions, and one of these regions will include the part 2 answer.
let intersectionPoint a b =
  let x1, y1, z1 = a.Pos
  let x2, y2, z2 = b.Pos
  let sizeRatio = float a.Radius / float (a.Radius + b.Radius)
  let x = (float x1 * (1. - sizeRatio)) + (float x2 * sizeRatio)
  let y = (float y1 * (1. - sizeRatio)) + (float y2 * sizeRatio)
  let z = (float z1 * (1. - sizeRatio)) + (float z2 * sizeRatio)
  int x, int y, int z

let neighbors (x, y, z) = List.ofSeq <| seq {
  for x in [x - 1; x; x + 1] do
    for y in [y - 1; y; y + 1] do
      for z in [z - 1; z; z + 1] do
        yield x, y, z }

let inRangeBotCount point = nanobots |> Seq.filter (fun x -> inRange x point) |> Seq.length

let rec expandTowardOrigin ((x, y, z), count) =
  let dx = if x < 0 then x + 1 else x - 1
  let cx = inRangeBotCount (dx, y, z)
  if cx >= count then
    expandTowardOrigin ((dx, y, z), cx)
  else
    let dy = if y < 0 then y + 1 else y - 1
    let cy = inRangeBotCount (x, dy, z)
    if cy >= count then
       expandTowardOrigin ((x, dy, z), cy)
     else
       let dz = if z < 0 then z + 1 else z - 1
       let cz = inRangeBotCount (x, y, dz)
       if cz >= count then
         expandTowardOrigin ((x, y, dz), cz)
       else
         (x, y, z), count

let intersectionPoints =
  nanobots
  |> List.collect (fun x -> nanobots |> List.map (intersectionPoint x))
  |> List.distinct
  |> List.map (fun x -> x, inRangeBotCount x)

let bestInRangeCount = intersectionPoints |> List.map snd |> List.max
let bestPoints = intersectionPoints |> List.filter (snd >> ((=) bestInRangeCount)) |> List.map expandTowardOrigin
let veryBest = bestPoints |> List.map (fst >> dist (0, 0, 0)) |> List.min
printfn "Part 2: %d" veryBest
