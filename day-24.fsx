#r "nuget: FParsec"
open FParsec

type Side = ImmuneSystem | Infection
type Reaction = WeakTo of string list | ImmuneTo of string list

type UnitGroup = {
  Side: Side
  mutable Count: int
  HP: int
  AttackPower: int
  AttackType: string
  Weaknesses: Set<string>
  Immunities: Set<string>
  Initiative: int
  mutable Target: UnitGroup option
  mutable TargetedBy: UnitGroup option }

let pword = many letter |>> Array.ofList |>> System.String
let pcommaSeparatedWords = sepBy pword (skipString ", ")

let preaction = choice [
  skipString "weak to " >>. pcommaSeparatedWords |>> WeakTo
  skipString "immune to " >>. pcommaSeparatedWords |>> ImmuneTo ]

let preactions = sepBy preaction (skipString "; ")
let preactionsInParens = skipString " (" >>. preactions .>> skipString ")"

let punitGroup side boost =
  pint32
  .>> skipString " units each with "
  .>>. pint32
  .>> skipString " hit points"
  .>>. (opt preactionsInParens |>> Option.defaultValue [])
  .>> skipString " with an attack that does "
  .>>. (pint32 |>> (+) boost)
  .>> skipString " "
  .>>. pword
  .>> skipString " damage at initiative "
  .>>. pint32
  .>> skipNewline
  |>> fun (((((cnt, hp), reactions), dmg), atk), init) -> {
    Side = side
    Count = cnt
    HP = hp
    AttackPower = dmg
    AttackType = atk
    Weaknesses = reactions |> List.collect (function | WeakTo x -> x | _ -> []) |> Set
    Immunities = reactions |> List.collect (function | ImmuneTo x -> x | _ -> []) |> Set
    Initiative = init
    Target = None
    TargetedBy = None }

let pfile boost =
  skipString "Immune System:"
  .>> skipNewline
  >>. many (punitGroup ImmuneSystem boost)
  .>> skipNewline
  .>> skipString "Infection:"
  .>> skipNewline
  .>>. many (punitGroup Infection 0)
  |>> fun (x, y) -> Set (x @ y)

let initialState boost =
  System.IO.File.ReadAllText "day-24-input.txt"
  |> run (pfile boost)
  |> function
     | Success (r, _, _) -> r
     | _ -> raise <| System.Exception()

let effectivePower group = group.AttackPower * group.Count

let damageDealt attacker defender =
  if defender.Immunities |> Set.contains attacker.AttackType then
    0
  elif defender.Weaknesses |> Set.contains attacker.AttackType then
    attacker |> effectivePower |> (*) 2
  else
    attacker |> effectivePower

let selectTarget groups group =
  let target =
    groups
    |> Seq.filter (fun x -> x.Side <> group.Side && x.Count > 0 && Option.isNone x.TargetedBy)
    |> Seq.filter (fun x -> damageDealt group x > 0)
    |> Seq.sortByDescending (fun x -> damageDealt group x, effectivePower x, x.Initiative)
    |> Seq.tryHead
  match target with
  | None -> group.Target <- None
  | Some x -> group.Target <- Some x; x.TargetedBy <- Some group

let selectTargets groups =
  groups
    |> Seq.sortByDescending (fun x -> effectivePower x, x.Initiative)
    |> Seq.filter (fun x -> x.Count > 0)
    |> Seq.iter (selectTarget groups)

let attack groups =
  let dealDamage group =
    let target = Option.get group.Target
    let unitsKilled = (damageDealt group target) / target.HP
    target.Count <- target.Count - unitsKilled
  groups
    |> Seq.sortByDescending (fun x -> x.Initiative)
    |> Seq.filter (fun x -> x.Count > 0 && Option.isSome x.Target)
    |> Seq.iter (fun x -> dealDamage x)

let fight groups =
  for group in groups do
    group.Target <- None
    group.TargetedBy <- None
  groups |> selectTargets
  groups |> attack

let getWinner groups =
  let goodGuys = groups |> Seq.filter (fun x -> x.Side = ImmuneSystem && x.Count > 0)
  let badGuys = groups |> Seq.filter (fun x -> x.Side = Infection && x.Count > 0)
  match goodGuys |> Seq.length, badGuys |> Seq.length with
  | 0, 0 -> raise <| System.Exception()
  | _, 0 -> Some (ImmuneSystem, goodGuys |> Seq.sumBy (fun x -> x.Count))
  | 0, _ -> Some (Infection, badGuys |> Seq.sumBy (fun x -> x.Count))
  | _ -> None

let isDeadlock groups =
  let goodGuys = groups |> Seq.filter (fun x -> x.Side = ImmuneSystem && x.Count > 0)
  let badGuys = groups |> Seq.filter (fun x -> x.Side = Infection && x.Count > 0)
  goodGuys
    |> Seq.collect (fun x -> badGuys |> Seq.map (fun y -> x, y))
    |> Seq.forall (fun (x, y) -> damageDealt x y = 0 && damageDealt y x = 0)

let combat groups =
  while ((getWinner groups |> Option.isNone) && not (groups |> isDeadlock)) do
    fight groups
  getWinner groups

printfn "Part 1: %d" (combat (initialState 0) |> Option.get |> snd)
[500 .. -1 .. 0]
  |> Seq.map (fun x -> combat (initialState x))
  |> Seq.takeWhile Option.isSome
  |> Seq.last
  |> Option.get
  |> snd
  |> printfn "Part 2: %d"
