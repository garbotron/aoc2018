let parseLine (str: string) = str.Split(',') |> fun x -> int x.[0], int x.[1], int x.[2], int x.[3]
let input = System.IO.File.ReadAllLines("day-25-input.txt") |> Array.map parseLine |> Set

let dist (x1, y1, z1, t1) (x2, y2, z2, t2) =
  let dim a b = abs (a - b)
  dim x1 x2 + dim y1 y2 + dim z1 z2 + dim t1 t2

// This is ugly but the functional solution was too slow...
let findAllConstellations stars =
  let mutable constellations = []
  let mutable stars = stars
  while not (stars |> Set.isEmpty) do
    let star = stars |> Seq.head
    let mutable unexplored = Set [star]
    let mutable constellation = Set.empty
    stars <- stars |> Set.remove star
    while not (unexplored |> Set.isEmpty) do
      let star = unexplored |> Seq.head
      constellation <- constellation |> Set.add star
      unexplored <- unexplored |> Set.remove star
      let inRange = stars |> Set.filter (fun x -> dist x star <= 3)
      unexplored <- unexplored |> Set.union inRange
      stars <- Set.difference stars inRange
    constellations <- constellation :: constellations
  constellations

printfn "Part 1: %d" (input |> findAllConstellations |> List.length)
